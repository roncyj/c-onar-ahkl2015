package com.example.angelhack2015;

/**
 * Created by Ron on 18/2/2015.
 */

import android.util.Log;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;

public class TCPClient {

    private String serverMessage;
    public static String SERVERIP;
    private OnMessageReceived mMessageListener = null;
    private boolean mRun = false;

    private static PrintWriter out;
    private static Socket socket;



    public TCPClient(OnMessageReceived listener) {
        mMessageListener = listener;
    }

    public TCPClient(String str) {
        SERVERIP = str;
    }

    public static void sendMessage(String message) {
        try {
            out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(
                    socket.getOutputStream())), true);
            MainActivity.ip = true;
        } catch (IOException e) {
            MainActivity.ip = false;
            e.printStackTrace();
        }
        if (out != null && !out.checkError()) {
            out.println(message);
            out.flush();
        }
    }

    public void stopClient() {
        mRun = false;
    }

    public void run() {
        // set the ip

        mRun = true;

        try {

            // Inetaddr to set the server ip
            InetAddress serverAddr = InetAddress.getByName(SERVERIP);

            Log.e("TCP Client", "C: Connecting...");
            // create a socket to make the connection with the server
            int SERVERPORT = 4444;
            socket = new Socket(serverAddr, SERVERPORT);
            MainActivity.ip = true;
            Log.e("TCP", "do5");
            try {

                // receive the message which the server sends back
                BufferedReader in = new BufferedReader(new InputStreamReader(
                        socket.getInputStream()));

                // in this while the client listens for the messages sent by the
                // server
                while (mRun) {
                    serverMessage = in.readLine();
                    if (serverMessage != null && mMessageListener != null) {
                        // call the method messageReceived from MyActivity class
                        mMessageListener.messageReceived(serverMessage);

                    }
                    serverMessage = null;

                }

                Log.e("RESPONSE FROM SERVER", "S: Received Message: '"
                        + serverMessage + "'");

            } catch (Exception e) {

                Log.e("TCP", "S: Error", e);

            } finally {
                socket.close();

            }

        } catch (Exception e) {

            Log.e("TCP", "C: Error", e);
            MainActivity.ip = false;
        }

    }

    public interface OnMessageReceived {
        public void messageReceived(String message);
    }

}
package com.example.angelhack2015;

import android.animation.ArgbEvaluator;
import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.NotificationCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.angelhack2015.R;
import com.immersion.uhl.Device;

import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends ActionBarActivity implements
        NavigationDrawerFragment.NavigationDrawerCallbacks {

    private NavigationDrawerFragment mNavigationDrawerFragment;
    private CharSequence mTitle;
    private static String ipaddr;
    static boolean ip = false;
    private TextView dist;
    private TextView vel;
    Button tgBtn;
    private NotificationManager mNotificationManager;
    private NotificationCompat.Builder mBuilder;
    String[] receiveData;
    private int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
                .findFragmentById(R.id.navigation_drawer);
        mTitle = getTitle();

        // Set up the drawer.
        mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
                (DrawerLayout) findViewById(R.id.drawer_layout));

        RelativeLayout reL = (RelativeLayout) findViewById(R.id.bgColor);
        tgBtn = (Button) findViewById(R.id.toggleButton);
        dist = (TextView) findViewById(R.id.distantText);
        vel = (TextView) findViewById(R.id.acceText);
        dist.setText("0 cm");
        vel.setText("0 km/h");
        initializeNotif(); // intialize Notfication

    }

    public void initializeNotif() {
        Intent dismissIntent = new Intent(this, MainActivity.class);
        PendingIntent piDismiss = PendingIntent.getService(this, 0,
                dismissIntent, 0);
        mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.ic_drawer).setAutoCancel(true)
                .setContentTitle("Sonar").setContentText("Hello")
                .setDefaults(Notification.DEFAULT_ALL);

        //Start initialize Notification
        Intent resultIntent = new Intent(this, MainActivity.class);
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(this);
        stackBuilder.addParentStack(MainActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(0,
                PendingIntent.FLAG_UPDATE_CURRENT);
        mBuilder.setContentIntent(resultPendingIntent);
        mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        //End of initialize
    }

    public void notiNew(String message) {

        mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.logo).setContentTitle("Sonar")
                .setContentText(message);

        // Vibration
        mBuilder.setVibrate(new long[] { 0, 250, 50, 250, 50, 1000 });
        mNotificationManager.notify(R.id.toggleButton, mBuilder.build());
    }

    private class connectTask extends AsyncTask<String, String, TCPClient> {

        @Override
        protected TCPClient doInBackground(String... message) {
            // we create a TCPClient object and
            TCPClient mTcpClient = new TCPClient(
                    new TCPClient.OnMessageReceived() {
                        @Override
                        // here the messageReceived method is implemented
                        public void messageReceived(String message) {
                            // this method calls the onProgressUpdate
                            publishProgress(message);
                        }
                    });

            TCPClient.SERVERIP = ipaddr;
            mTcpClient.run();
            return null;
        }

        @Override
        protected void onProgressUpdate(String... values) {
            super.onProgressUpdate(values);
            Device mDevice = Device.newDevice(getApplicationContext());
            if (values[0].equals("1")) {
                ip = true;
                msbox2("SERVER REPLY", "Connection Success!");
            } else {
                receiveData = values[0].split("[;]");
                Log.e("Test", "Length : " + String.valueOf(receiveData.length));
                if (receiveData.length == 4) {
                    if (Integer.parseInt(receiveData[0]) == 1) {
                        if (Boolean.parseBoolean(receiveData[1]) == true) {
                            notiNew("Please move forward");
                        }
                    } else {
                        ObjectAnimator colorFade;
                        if (Boolean.parseBoolean(receiveData[1]) == true) {
                            colorFade = ObjectAnimator
                                    .ofObject(
                                            (RelativeLayout) findViewById(R.id.bgColor),
                                            "backgroundColor",
                                            new ArgbEvaluator(),
                                            Color.argb(255, 255, 255, 255),
                                            0xff00ff00);
                            if (flag != 1) {
                                colorFade.setDuration(1000);
                                colorFade
                                        .setRepeatCount(ValueAnimator.INFINITE);
                                colorFade.setRepeatMode(ValueAnimator.REVERSE);
                                colorFade.start();
                            }
                            flag = 1;
                        } else {
                            colorFade = ObjectAnimator
                                    .ofObject(
                                            (RelativeLayout) findViewById(R.id.bgColor),
                                            "backgroundColor",
                                            new ArgbEvaluator(),
                                            Color.argb(255, 255, 255, 255),
                                            0xffff0000);
                            if (flag != 2) {
                                colorFade.setDuration(1000);
                                colorFade
                                        .setRepeatCount(ValueAnimator.INFINITE);
                                colorFade.setRepeatMode(ValueAnimator.REVERSE);
                                colorFade.start();
                            }
                            flag = 2;
                        }

                    }
                    dist.setText(receiveData[2] + " cm");
                    vel.setText(receiveData[3] + " km/h");
                }
            }
        }
    }

    void msbox2(String str, String str2) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(str);
        builder.setMessage(str2);
        builder.setCancelable(true);

        final AlertDialog dlg = builder.create();

        dlg.show();
        final Timer t = new Timer();
        t.schedule(new TimerTask() {
            public void run() {
                // when the task active then close the dialog
                dlg.dismiss();
                // also just top the timer thread, otherwise, you
                // may receive a crash report
                t.cancel();
            }
        }, 2000); // after 1 second, the task activated
    }

    @Override
    public void onNavigationDrawerItemSelected(int position) {
        // update the main content by replacing fragments
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager
                .beginTransaction()
                .replace(R.id.container,
                        PlaceholderFragment.newInstance(position + 1)).commit();
    }

    public void getIP(String ipadd) {

        final TextView ipAddress = (TextView) findViewById(R.id.ipAddress);
        ipaddr = ipadd;
        new connectTask().execute("");
        if (!ip) {
            msbox2("SERVER CONNECTION ERROR",
                    "Please check your network connection and server IP address.");
        }
    }

    void onSectionAttached(int number) {
        Context context = getApplicationContext();
        CharSequence text;
        int duration = Toast.LENGTH_LONG;
        Toast toast;
        switch (number) {
            case 1:
                getIP("192.168.240.229");
                break;
        }
    }

    void restoreActionBar() {
        ActionBar actionBar = getSupportActionBar();
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setTitle(mTitle);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (!mNavigationDrawerFragment.isDrawerOpen()) {
            // Only show items in the action bar relevant to this screen
            // if the drawer is not showing. Otherwise, let the drawer
            // decide what to show in the action bar.
            getMenuInflater().inflate(R.menu.main, menu);
            restoreActionBar();
            return true;
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        // noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_SECTION_NUMBER = "section_number";

        /**
         * Returns a new instance of this fragment for the given section number.
         */
        public static PlaceholderFragment newInstance(int sectionNumber) {
            PlaceholderFragment fragment = new PlaceholderFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_SECTION_NUMBER, sectionNumber);
            fragment.setArguments(args);
            return fragment;
        }

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container,
                    false);
            return rootView;
        }

        @Override
        public void onAttach(Activity activity) {
            super.onAttach(activity);
            ((MainActivity) activity).onSectionAttached(getArguments().getInt(
                    ARG_SECTION_NUMBER));
        }
    }

}

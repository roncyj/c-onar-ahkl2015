package angelhack2015; 
public class ThreeSecondRule {
	private double velocity;//cm/s
	private double safedistance; //cm
	private int time; //s
	
	public ThreeSecondRule(){
		velocity = 0;
		safedistance = 0;
		time = 3;
	}
	
	public int getTime(){
		return time;
	}
	
	public void setTime(int time){
		this.time = time;
	}
	
	public double getVelocity(){
		return velocity;
	}
	
	public void setVelocity(double velocity){
		this.velocity = velocity;
	}
	
	public void setSafeDistance(double safedistance){
		this.safedistance = safedistance;
	}
	
	public double getSafeDistance(){
		return safedistance/100;
	}
	
	public boolean threeSecondCalculation(double distance){
		setSafeDistance(velocity * time);
		if(distance >= safedistance)
			return true;
		else
			return false;
	}
}

package angelhack2015;

import java.net.*;
import java.io.*;
import java.util.*;

import com.jcraft.jsch.JSchException;


public class BroadcastServer extends Thread {
	protected static boolean serverContinue = true;
	protected static Socket clientSocket;
	protected Shared outputStreamList;

	BroadcastServer(Socket clientSoc, Shared s) {
		clientSocket = clientSoc;
		outputStreamList = s;

		start();
	}

//	public static String receivedMessage() throws IOException, JSchException {
//		BufferedReader in = new BufferedReader(new InputStreamReader(
//				clientSocket.getInputStream()));
//
//		return check;
//	}

	public void run() {
		System.out.println("New Communication Thread Started");

		try {
			PrintWriter out = new PrintWriter(clientSocket.getOutputStream(),
					true);
			outputStreamList.addStream(out);
//			receivedMessage();
		} catch (IOException e) {
			System.err.println("Problem with Communication Server");
			System.exit(1);
		}
	}
}

class Shared {
	private static Vector<PrintWriter> outputStreamList;

	public Shared() {
		outputStreamList = new Vector();
	}

	synchronized void addStream(PrintWriter outStream) {
		if (!(outputStreamList.contains(outStream))) {
			outputStreamList.addElement(outStream);
			System.out.println("Registered new client ");
			sendMessageToStreams("1");
		} // end if

	}
	
	synchronized static void sendMessageToStreams(String s) {
		// Loop for streams in the vector and write s to each stream
		System.out.println("**************************************\n"
				+ "Broadcast initiated ---");

		for (int i = 0; i < outputStreamList.size(); i++) {
			System.out.println("doing " + i + "-th callback\n");
			// convert the vector object to a callback object
			PrintWriter outStream = (PrintWriter) outputStreamList.elementAt(i);
			// invoke the callback method
			outStream.println(s);
		}// end for

		System.out.println("********************************\n"
				+ "Server completed Broadcast ---");
	}

	synchronized void removeStream(PrintWriter outStream) {
		// remove the stream from the vector
		if (outputStreamList.removeElement(outStream)) {
			System.out.println("Unregistered client ");
		} else {
			System.out.println("Unregister: client wasn't registered.");
		}
	}
}

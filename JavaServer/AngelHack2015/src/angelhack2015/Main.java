package angelhack2015;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class Main {
	public static final int SERVERPORT = 4444;
	// determine running server
	protected static boolean serverContinue = true;
	protected Socket clientSocket;
	protected Shared outputStreamList;
	static private int finalsensor = 0; // final sensor distance value
	static private GUI gui;
	static private TrafficGUI tgui;
	static private String output = "";
	static private boolean flag = true;

	static public int getfinalsensor() {
		return finalsensor;
	}

	public static void main(String[] args) throws IOException,
			InterruptedException {
		ArduinoServer arduino = new ArduinoServer();
		arduino.initialize();
		Thread.sleep(1000);
		MainGUI maingui = new MainGUI(); // velocity slider
		ServerSocket serversocket = null;
		Shared share = new Shared();
		serversocket = new ServerSocket(SERVERPORT);
		BroadcastServer broadcast = new BroadcastServer(serversocket.accept(),
				share);
		BroadcastServer broadcast1 = new BroadcastServer(serversocket.accept(),
				share);
		while (true) {
			int[] data = arduino.getData();
			finalsensor = detfinalsensor(data);

			if (MainGUI.getGUI()) {
				if (gui == null) {
					gui = new GUI();
				}
				output = gui.stateChange(finalsensor);
				share.sendMessageToStreams(output + ";" + finalsensor + ";"
						+ maingui.source.getValue());
				System.out.println(output + ";" + finalsensor + ";"
						+ maingui.source.getValue());

				if (tgui != null) {
					tgui.dispose();
					tgui = null;
				}
			}
			if (MainGUI.getTGUI()) {
				if (tgui == null) {
					tgui = new TrafficGUI();
				}
				output = tgui.stateChange(finalsensor);
				share.sendMessageToStreams(output + ";" + finalsensor + ";"
						+ maingui.source.getValue());
				System.out.println(output + ";" + finalsensor + ";"
						+ maingui.source.getValue());

				if (gui != null) {
					gui.dispose();
					gui = null;
				}
			}
		}
	}

	static public int detfinalsensor(int[] sensor) {
		int sfinal = 0;
		if (sensor[0] < sensor[1]) {
			sfinal = sensor[0];
		} else
			sfinal = sensor[1];
		return sfinal;
	}
}

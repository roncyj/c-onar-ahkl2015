package angelhack2015;

//jsch is an open source SSH client for Java. 
//get it from <a href="http://www.jcraft.com/jsch/" target="_blank" rel="nofollow">http://www.jcraft.com/jsch/</a>
import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.ChannelSftp;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.Channel;

import java.io.*;

public class ArduinoServer {

	private byte[] tmp = new byte[1024];
	private InputStream in;
	private Channel channel;
	private Session session;

	public void initialize() {

		// Arduino Information
		String user = "root";
		String password = "arduino";
		String host = "192.168.240.1";
		int port = 22;

		try {

			JSch jsch = new JSch();
			session = jsch.getSession(user, host, port);
			session.setPassword(password);
			session.setConfig("StrictHostKeyChecking", "no"); // less than
																// maximally
																// secure
			System.out.println("Establishing Connection...");
			session.connect();
			System.out.println("Connection established.");
			channel = session.openChannel("exec");

			// this stream sends data to the Arduino Yun
			DataOutputStream dataOut = new DataOutputStream(
					channel.getOutputStream());
			channel.setInputStream(null);

			// stuff
			((ChannelExec) channel).setCommand("telnet localhost 6571");
			((ChannelExec) channel).setErrStream(System.err);

			// this stream receives data from the Arduino Yun
			in = channel.getInputStream();

			// connect to the Arduino Yun
			channel.connect();

		} catch (Exception e) {
			System.err.print(e);
		}

	}

	public int[] getData() throws IOException {
		String data = null;
		// poll input stream
		while (true) {
			while (in.available() > 0) {
				int j = in.read(tmp, 0, 1024);
				if (j < 0)
					break;
				try {
					String inputLine = new String(tmp, 0, j);
					String send = "";
					String[] splitArray = inputLine.split(";");
					int[] sensor = new int[2];
					for (int i = 0; i < splitArray.length; i++)
						sensor[i] = Integer.parseInt(splitArray[i]);
					return sensor;
				} catch (Exception e) {
					// System.err.println(e.toString());
				}
			}

			if (channel.isClosed()) {
				System.out.println("Exit Status: " + channel.getExitStatus());
				break;
			}
		}
		return null;

	}

	public void disconnect() {
		// Disconnect the channel
		System.out.println("Disconnecting...");
		channel.disconnect();
		session.disconnect();
		System.out.println("Disconnected.\n");
	}
}

package angelhack2015;
import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class MainGUI extends JFrame{
	private JPanel panel;
	private JPanel panel1;
	private JLabel label;
	private JSlider slider;
	private JTextField text;
	private static boolean gui,tgui;
	private static ThreeSecondRule TRule;
	public JSlider source ;
	
	public MainGUI(){
		super("Car Distance Detection System");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		panel = new JPanel();
		panel1 = new JPanel();
		
		label = new JLabel("Velocity");
		
		text = new JTextField("0km/h");
		text.setColumns(5);
		
		slider = new JSlider(JSlider.HORIZONTAL,0,300,0);
		slider.setMinorTickSpacing(10);
		slider.setMajorTickSpacing(50);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.addChangeListener(new SliderListener());
		
		panel.add(label);
		panel.add(text);
		panel1.add(slider);
		panel1.setLayout(new BoxLayout(panel1, BoxLayout.Y_AXIS));
		
		add(panel,BorderLayout.NORTH);
		add(panel1,BorderLayout.SOUTH);
		
		pack();
		setVisible(true);
	}
	public static boolean getGUI(){
		return gui;
	}
	public static boolean getTGUI(){
		return tgui;
	}
	public static ThreeSecondRule getTRule(){
		return TRule;
	}
	
	class SliderListener implements ChangeListener{
		public void stateChanged(ChangeEvent e){
			TRule = new ThreeSecondRule();
			source = (JSlider)e.getSource();
			TRule.setVelocity(source.getValue()*28);
			if(source.getValueIsAdjusting()){
				text.setText(String.valueOf(source.getValue()) + "km/h");
				if(source.getValue() > 0){
					gui=true;
					tgui=false;	
					
				}
				else if(source.getValue() <= 0){
					tgui=true;
					gui=false;
				}								
			}
		}
	}
}



package angelhack2015;

public class Traffic {	
	private int distance=0; //distance is in CM
			
	public void setDistance(int d){
		distance=d;
	}
	public int getDistance(){
		return distance;
	}
	
	public boolean carIsmoving(int d){
		if (distance==0){
			distance=d;
			return false;
		}
		else if((d-this.getDistance())>50){
			return true;
		}
		else
			return false;
	}	
}

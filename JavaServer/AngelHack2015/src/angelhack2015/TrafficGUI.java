package angelhack2015;

import java.awt.BorderLayout;
import java.awt.GridLayout;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

public class TrafficGUI extends JFrame {
	int distance_MAX;
	int distance_MIN;
	int distance_INIT;
	int distance;
	boolean firsttime = true;
	JTextField sdistance = new JTextField(3); // slider distance/final distance
	JTextField result = new JTextField(10);
	JTextField initiald = new JTextField(3);
	JTextField finald = new JTextField(3);
	JPanel panel = new JPanel();
	JPanel panel1 = new JPanel(new BorderLayout());
	JPanel panel2 = new JPanel(new BorderLayout());
	JPanel panel3 = new JPanel();
	JLabel distanceL = new JLabel("Distance between the Front Car and Yours: ");
	JLabel carMove = new JLabel("Is the car in front Moving?");
	JLabel initial = new JLabel("Initial Distance");
	JLabel dfinal = new JLabel("Final Distance");
	Traffic traffic = new Traffic();

	TrafficGUI() {
		super("Anti-Traffic");
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		distance_MAX = 1000; // distance under Meter
		distance_MIN = 0;
		distance_INIT = 10;
		distance = distance_INIT;
		sdistance.setFocusable(false);
		initiald.setFocusable(false);
		finald.setFocusable(false);
		result.setFocusable(false);
		sdistance.setText(String.valueOf(distance));

		panel.add(distanceL);
		panel.add(sdistance);

		panel2.add(carMove, BorderLayout.NORTH);
		panel2.add(result, BorderLayout.SOUTH);
		panel3.add(initial);
		panel3.add(initiald);
		panel3.add(dfinal);
		panel3.add(finald);
		setLayout(new GridLayout(4, 0));
		add(panel);
		add(panel1);
		add(panel2);
		add(panel3);
		firsttime = true;
		pack();
		setVisible(true);
	}

	public String stateChange(int d) {
		distance = d;
		sdistance.setText(String.valueOf(distance));
		if (firsttime) {
			initiald.setText(String.valueOf(distance));
			traffic.setDistance(distance);
			firsttime = false;
			return "";
		} else {
			finald.setText(String.valueOf(distance));
			if (traffic.carIsmoving(distance)) {
				result.setText("ALERT!! It is MOVING!!!");
				return "1;true";
			} else {
				result.setText("Nope it's not moving");
				return "1;false";
			}
		}
	}

}

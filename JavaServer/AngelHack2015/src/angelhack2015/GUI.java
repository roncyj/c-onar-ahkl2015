package angelhack2015;
import java.awt.BorderLayout;

import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class GUI extends JFrame {
	private JTextField text;
	private JTextField text1;
	private JTextField text2;
	private JPanel panel;
	private JPanel panel1;
	private JLabel label;
	private JLabel label1;
	private JLabel label2;
	
	public GUI(){
		super("Velocity > 0");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		label = new JLabel("Distance between your car and the car infront");
		label1 = new JLabel("Obey three second rule?");
		label2 = new JLabel("Safe Distance");
		
		text = new JTextField();
		text.setEditable(false);
		
		text1 = new JTextField("0m");
		text1.setColumns(5);
		text1.setEditable(false);
		
		text2 = new JTextField();
		text2.setEditable(false);
		
			
		panel1 = new JPanel();
		panel1.add(label);
		panel1.add(text1);
		
		panel = new JPanel();		
		panel.add(label1);
		panel.add(text);
		panel.add(label2);
		panel.add(text2);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		add(panel1,BorderLayout.NORTH);
		add(panel,BorderLayout.SOUTH);
		pack();
		setVisible(true);
	}
	public String stateChange(int d){
		text.setText(String.valueOf(MainGUI.getTRule().threeSecondCalculation(d)));
		text1.setText(d + "cm");
		text2.setText(String.valueOf(MainGUI.getTRule().getSafeDistance()));
		return "2;"+String.valueOf(MainGUI.getTRule().threeSecondCalculation(d));	
	}
	
}

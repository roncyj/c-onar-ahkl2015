# README #

At its most basic function, this project offers frontal rangefinder data to detect distance between the car and any object in front of it. Rangefinder data is collected from an Arduino Yun, and transmitted over SSH to a micro-controller. These data unlocks many opportunities in creating a safer driving environment. Implementations include:

## Ambient Light System ##
An ambient light system can be installed in the car to indicate a ‘safe driving distance’ according to the 3-second rule. As drivers break the 3-second rule, the ambient light gradually becomes more red to inform the driver that he or she is driving dangerously. At a ‘safe driving distance’ the ambient light becomes a comforting green colour. Way to go, driver!

The ambient lighting system also serves to extend the driver's situational awareness. Immensely helpful if you resort to using your phone while waiting in traffic/traffic lights (we don't recommend it though). Sadly, this leads to less opportunities to honk at oblivious cars.

## Frontal Park Assist ##
The device's ease of installation makes it easy for any drivers to extend their car with a frontal park assist. With a simple mobile application, the driver can ‘see’ objects in front of their car that can’t however be seen with their eyes.